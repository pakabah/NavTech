package com.navtech.navtech.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pakabah on 25/11/2016.
 */

public class User {

    private String userEmail;
    private String userName;
    private boolean isLoggedIn;
    private String userId;
    private Context context;
    private static final String DEFAULT = "N/A";

    public User(Context context)
    {
        this.context = context;
    }

    public String getUserEmail() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        this.userEmail =  sharedPreferences.getString("useremail", DEFAULT);
        return this.userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        this.userName =  sharedPreferences.getString("username", DEFAULT);
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", userName);
        editor.apply();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        this.userId =  sharedPreferences.getString("userId", DEFAULT);
        if(!userId.equals(DEFAULT))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public String getUserId() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        this.userId =  sharedPreferences.getString("userId", DEFAULT);
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        SharedPreferences sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userId", userId);
        editor.apply();
    }
}
