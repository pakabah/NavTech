package com.navtech.navtech.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.navtech.navtech.db.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pakabah on 25/11/2016.
 */

public class Journey {

    private float destinationLong,destinationLat;

    private String destinationId,destinationName;

    private Context context;

    private String BASE_URL = "http://royalcrownghana.com/navtech/cms/ajax.php";
    private String ROOM_URL = "http://royalcrownghana.com/navtech/cms/app.php";

    public Journey(Context context) {
        this.context = context;
    }

    public void search(String destination) {

    }

    public void bookmarkJourney() {

    }

    public void shareJourney() {

    }

    public void addDestination(String destinationName, String destinationLong, String destinationLat,int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.Places.COLUMN_PLACE_NAME, destinationName);
        contentValues.put(DBHelper.Places.COLUMN_PLACE_LAT, destinationLat);
        contentValues.put(DBHelper.Places.COLUMN_PLACE_LONG, destinationLong);
        contentValues.put(DBHelper.Places.COLUMN_ID, id);

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.insertPlaces(contentValues);
    }

    public void addLayout(String placeId, String floor, String layout){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_PLACE_ID,placeId);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_FLOOR,floor);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_PICTURE,layout);

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.insertLayout(contentValues);
    }

    public void addLayoutWithFloorId(String placeId, String floor, String layout,String floorId){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_PLACE_ID,placeId);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_FLOOR,floor);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_PICTURE,layout);
        contentValues.put(DBHelper.Places.COLUMN_FLOOR_ID,floorId);

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.insertLayout(contentValues);
    }

    public void addRooms(String room_id,String floor_id,String room_name,String place_id){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.Places.COLUMN_ROOM_NAME,room_name);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_ROOM_ID,room_id);
        contentValues.put(DBHelper.Places.COLUMN_FLOOR_ID,floor_id);
        contentValues.put(DBHelper.Places.COLUMN_LAYOUT_PLACE_ID,place_id);

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.insertRooms(contentValues);
    }

    static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public void deleteLocations(){

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.deleteAllPlaces();
    }


    public void getAllLocations() {
        class getLocationsAsync extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL(BASE_URL);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(null != s && !s.isEmpty()) {
                    Log.e("Server Message", s);

                    try {

                        JSONArray jsonArray = new JSONArray(s);

                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String latitude = jsonObject.getString("latitude");
                            String longitude = jsonObject.getString("longitude");
                            String building_name = jsonObject.getString("name_of_building");
                            int id = Integer.parseInt(jsonObject.getString("id")) ;

                            addDestination(building_name,longitude,latitude,id);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        getLocationsAsync getLocationsAsync = new getLocationsAsync();
        getLocationsAsync.execute();
    }

    public void getBuildingDetails(final String location_id){

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.deleteAllLayouts();

        class getBuildingDetailsAsync extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String paramLocationId = params[0];

                try {
                    URL url = new URL(BASE_URL+"?location_id="+paramLocationId);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(null != s && !s.isEmpty()) {
                    Log.e("Server Message", s);

                    try {

                        JSONArray jsonArray = new JSONArray(s);

                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String floor = jsonObject.getString("floor");
                            String layout = jsonObject.getString("layout");

                            addLayout(location_id,floor,layout);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        getBuildingDetailsAsync getBuildingDetailsAsyncs = new getBuildingDetailsAsync();
        getBuildingDetailsAsyncs.execute(location_id);
    }

    public void getFloorDetails(String location_id,String floor_id){

        class getFloorDetailsAsync extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String paramLocationId = params[0];
                String paramFloorId = params[1];

                try {
                    URL url = new URL(ROOM_URL+"?location_id="+paramLocationId+"&floor_id="+paramFloorId);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(null != s && !s.isEmpty()) {
                    Log.e("Server Message Floor", s);

                    try {

                        JSONArray jsonArray = new JSONArray(s);

                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String floor = jsonObject.getString("floor_name");
                            String layout = jsonObject.getString("layout");
                            String location_id = jsonObject.getString("location_id");
                            String floor_id = jsonObject.getString("floor_id");

                            addLayoutWithFloorId(location_id,floor,layout,floor_id);
                        }

                        publishRoomFloor("200",context);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        getFloorDetailsAsync getFloorDetailsAsync = new getFloorDetailsAsync();
        getFloorDetailsAsync.execute(location_id,floor_id);

    }

    public void getRooms(final String location_id){
//        DBHelper dbHelper = new DBHelper(context);
//        dbHelper.deleteAllRooms();

        class getRoomsAsync extends AsyncTask<String,Void,String>{


            @Override
            protected String doInBackground(String... params) {
                String paramLocationId = params[0];

                try {
                    URL url = new URL(ROOM_URL+"?location_id="+paramLocationId);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(null != s && !s.isEmpty()) {
                    Log.e("Server Message", s);

                    try {

                        JSONArray jsonArray = new JSONArray(s);

                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String room_id = jsonObject.getString("room_id");
                            String floor_id = jsonObject.getString("floor_id");
                            String room_name = jsonObject.getString("room_name");

                            addRooms(room_id,floor_id,room_name,location_id);
                        }

                        publishRoom("200",context);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        getRoomsAsync getRoomsAsync = new getRoomsAsync();
        getRoomsAsync.execute(location_id);
    }

    public static String ROOM = "android.intent.action.navtech_room";

    public static String ROOM_FLOOR = "android.intent.action.navtech_floor";



    public static void publishRoom(String Result,Context context){
        Intent intent = new Intent(ROOM);
        intent.setAction(ROOM);
        intent.putExtra("status", Result);
        context.sendBroadcast(intent);
    }


    public static void publishRoomFloor(String Result,Context context){
        Intent intent = new Intent(ROOM_FLOOR);
        intent.setAction(ROOM_FLOOR);
        intent.putExtra("status", Result);
        context.sendBroadcast(intent);
    }

}

