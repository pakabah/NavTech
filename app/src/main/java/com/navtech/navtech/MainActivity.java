package com.navtech.navtech;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.navtech.navtech.core.CustomAuto;
import com.navtech.navtech.view.CustomAutoView;
import com.navtech.navtech.view.LocationDetails;
import com.navtech.navtech.view.Login;

public class MainActivity extends AppCompatActivity {

    String login;
    public static final String DEFAULT = "N/A";
    public CustomAutoView customAutoView, fromCustomAutoView;
    public String[] item = new String[] {"Please search..."};
    public String[] items = new String[] {"Please search..."};
    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> adapters;
    String fromTerm = "Current Location";
    private LocationManager lm;
    boolean isLocationOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fromTerm = "Current Location";
        final Button search = (Button) findViewById(R.id.searchButton);

        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        login = sharedPreferences.getString("login", DEFAULT);

        if(!login.equals("1")) {
            Intent intent = new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            finish();
        }

        customAutoView = (CustomAutoView) findViewById(R.id.search);
        customAutoView.addTextChangedListener(new CustomAuto(this));

        fromCustomAutoView = (CustomAutoView) findViewById(R.id.from);
        fromCustomAutoView.addTextChangedListener(new CustomAuto(this));

        lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            fromCustomAutoView.setVisibility(View.VISIBLE);
            isLocationOn = false;
        }else {
            fromCustomAutoView.setVisibility(View.GONE);
            isLocationOn = true;
        }

        adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,item);
        adapters = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,item);

        customAutoView.setAdapter(adapter);
        fromCustomAutoView.setAdapter(adapters);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             String searchTerm = customAutoView.getText().toString();

                if(searchTerm.isEmpty()) {
                    Toast.makeText(getApplicationContext(),"Please Enter where you want to go", Toast.LENGTH_LONG).show();
                }else if(!isLocationOn && !fromCustomAutoView.getText().toString().isEmpty()){
                    fromTerm = fromCustomAutoView.getText().toString();
                    searching();
                }
                else {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{
                                    android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET
                            }, 1211);
                        }
                        return;
                    }
                    else {
                        searching();
                    }
                }
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.imageView3);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void searching() {
        Intent intent = new Intent(getApplicationContext(), LocationDetails.class);
        intent.putExtra("searchTerm", customAutoView.getText().toString());
        intent.putExtra("searchFrom", fromTerm);
        startActivity(intent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case 1211:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    searching();
                }

                break;
        }
    }
}