package com.navtech.navtech.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.navtech.navtech.R;

import java.io.FileNotFoundException;

public class ZoomView extends AppCompatActivity {

    SubsamplingScaleImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_view);

        imageView = (SubsamplingScaleImageView)findViewById(R.id.imageView);

        try {
            Bitmap bitmap = BitmapFactory.decodeStream(getApplicationContext().openFileInput("myImage"));
            imageView.setImage(ImageSource.bitmap(bitmap));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
