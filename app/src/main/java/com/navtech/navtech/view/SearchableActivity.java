package com.navtech.navtech.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.navtech.navtech.R;

public class SearchableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
    }
}
