package com.navtech.navtech.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.Constants;
import com.mapbox.services.commons.ServicesException;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.mapbox.services.directions.v5.DirectionsCriteria;
import com.mapbox.services.directions.v5.MapboxDirections;
import com.mapbox.services.directions.v5.models.DirectionsResponse;
import com.mapbox.services.directions.v5.models.DirectionsRoute;
import com.navtech.navtech.MainActivity;
import com.navtech.navtech.R;
import com.navtech.navtech.db.DBHelper;
import com.navtech.navtech.model.Journey;
import com.navtech.navtech.template.MainTemplate;
import com.navtech.navtech.template.RoomTemplate;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Navigation extends AppCompatActivity {

    private MapView mapView;
    private MapboxMap map;
    private DirectionsRoute currentRoute;
    private DirectionsRoute altRoute;
    private static final String TAG = "NavigationActivity";
    Button enterBuilding,endTrip;
    private static final int PERMISSIONS_LOCATION = 0;
    private FloatingActionButton floatingActionButton;
    private LocationServices locationServices;
    ProgressDialog progress;
    MainTemplate mainTemp,mainTemplate;

    Position origin;
    Position destination;
    String originTemp;
    boolean is_CurrentLocation = false;
    DBHelper dbHelper;
    public CustomAutoView customAutoView;
    public String[] item = new String[] {"Please search..."};
    public ArrayAdapter<String> adapter;
    Spinner rooms;
    Button search;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {
                String status = bundle.getString("status");
                assert status != null;

                loadSpinnerData(mainTemplate.Id);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        registerReceiver(broadcastReceiver, new IntentFilter(Journey.ROOM));

        String token = "pk.eyJ1Ijoic3BlY2tvcmJpdGVyIiwiYSI6ImNpdjUweXF4djAwMDgydHFteXpsa3M4MTIifQ.rnyZJm6XA6tIjXGA6wBuAQ";
        MapboxAccountManager.start(this, token);

        Bundle bundle = getIntent().getExtras();
        final String place = bundle.getString("searchTerm");
        originTemp = bundle.getString("searchFrom");


        dbHelper = new DBHelper(getApplicationContext());
         mainTemplate =  dbHelper.getSinglePlace(place);

        rooms = (Spinner) findViewById(R.id.spinner);
        search = (Button) findViewById(R.id.search);

        destination = Position.fromCoordinates(Double.parseDouble(mainTemplate.Long),Double.parseDouble(mainTemplate.Lat));

        locationServices = LocationServices.getLocationServices(Navigation.this);

        enterBuilding = (Button) findViewById(R.id.button3);
        endTrip = (Button) findViewById(R.id.button4);

        Journey journey = new Journey(getApplicationContext());
        journey.getBuildingDetails(mainTemplate.Id);

        enterBuilding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),PlanView.class);
                intent.putExtra("searchId", mainTemplate.Id);
                startActivity(intent);
            }
        });

        endTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.location_toggle_fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (map != null) {
                    toggleGps(!map.isMyLocationEnabled());
                }
            }
        });

        if(!originTemp.equals("Current Location")){
            is_CurrentLocation = true;
            mainTemp = dbHelper.getSinglePlace(originTemp);
            origin = Position.fromCoordinates(Double.parseDouble(mainTemp.Long),Double.parseDouble(mainTemp.Lat));
            Log.e("Search From","Checking origin......");
        }
//        if (map != null) {
//            toggleGps(!map.isMyLocationEnabled());
//        }

//        Location lastLocation = locationServices.getLastLocation();
//        origin = Position.fromCoordinates(lastLocation.getLongitude(),lastLocation.getLatitude());


        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;

                mapboxMap.setMyLocationEnabled(true);

                progress = new ProgressDialog(Navigation.this);
                progress.setTitle("Navigation");
                progress.setMessage("Loading Map...");
                progress.show();



                if(map.getMyLocation() != null) {
                    // Set the origin as user location only if we can get their location
                    origin = Position.fromCoordinates(map.getMyLocation().getLongitude(), map.getMyLocation().getLatitude());
                    progress.dismiss();
                }else{
                    Log.e("Search From","Checking origin...... "+mainTemp.Long);

                    origin = Position.fromCoordinates(Double.parseDouble(mainTemp.Long),Double.parseDouble(mainTemp.Lat));
//                    Toast.makeText(getApplicationContext(),"Could not get current Location", Toast.LENGTH_LONG).show();
                    progress.dismiss();
                }

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(destination.getLatitude(), destination.getLongitude())).build();
                mapboxMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(origin.getLatitude(), origin.getLongitude()))
                        .title("Origin")
                        .snippet("Origin"));
                mapboxMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(destination.getLatitude(), destination.getLongitude()))
                        .title("Destination")
                        .snippet(mainTemplate.Name));

                mapboxMap.setCameraPosition(cameraPosition);


                try {
                    getRoute(origin, destination);
                    Log.e("Search From","Getting Route...... ");
                } catch (ServicesException servicesException) {
                    servicesException.printStackTrace();
                }
            }
        });

        locationServices.addLocationListener(new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(!is_CurrentLocation){
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.
                        origin = Position.fromCoordinates(location.getLongitude(),location.getLatitude());
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 13));
                    }
                }
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedRoom = rooms.getSelectedItem().toString();
                if(selectedRoom.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please Select a Room", Toast.LENGTH_LONG).show();
                }else{
                    DBHelper dbHelper = new DBHelper(getApplicationContext());
                    RoomTemplate roomTemplate = dbHelper.getRoomId(selectedRoom);
//                    Log.e("Floor ID", roomTemplate.floorId);
                    Journey journey1 = new Journey(getApplicationContext());
                    journey1.getFloorDetails(mainTemplate.Id,roomTemplate.floorId);

                    Intent intent = new Intent(getApplicationContext(),PlanView.class);
                    intent.putExtra("searchId", mainTemplate.Id);
                    intent.putExtra("searchFloor", roomTemplate.floorId);
                    startActivity(intent);
                }
            }
        });
    }

    private void loadSpinnerData(String layout_id){

        DBHelper dbHelper = new DBHelper(getApplicationContext());
        List<String> roomTemplates = dbHelper.getRoomsString(layout_id);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, roomTemplates);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        rooms.setAdapter(dataAdapter);
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
            // Check if user has granted location permission
            if (!locationServices.areLocationPermissionsGranted()) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            } else {
                enableLocation(true);
            }
        } else {
            enableLocation(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableLocation(true);
            }
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();
            if (lastLocation != null) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
            }

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationServices.removeLocationListener(this);
                    }
                }
            });
            floatingActionButton.setImageResource(R.drawable.location_off);
        } else {
            floatingActionButton.setImageResource(R.drawable.define_location);
        }
        // Enable or disable the location layer on the map
        map.setMyLocationEnabled(enabled);
    }

    private void getRoute(Position origin, Position destination) throws ServicesException {

        MapboxDirections client = new MapboxDirections.Builder()
                .setOrigin(origin)
                .setDestination(destination)
                .setAlternatives(true)
                .setProfile(DirectionsCriteria.PROFILE_CYCLING)
                .setAccessToken(MapboxAccountManager.getInstance().getAccessToken())
                .build();

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                // You can get the generic HTTP info about the response
                Log.e(TAG, "Response code: " + response.code());
                if (response.body() == null) {
                    Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().getRoutes().size() < 1) {
                    Log.e(TAG, "No routes found");
                    return;
                }

                // Print some info about the route
                currentRoute = response.body().getRoutes().get(0);

                if(response.body().getRoutes().size() > 1){
                    Log.e("Alternative route", "alternative");
                    altRoute = response.body().getRoutes().get(1);
                    Log.d(TAG, "Distance: " + altRoute.getDistance());
                    drawAlternativeRoute(altRoute);
                }


//                Log.e(TAG, "Distance: " + currentRoute.getDistance());
//                Toast.makeText(
//                        Navigation.this,
//                        "Route is " + currentRoute.getDistance() + " meters long.",
//                        Toast.LENGTH_SHORT).show();

                // Draw the route on the map
                drawRoute(currentRoute);
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                Log.e(TAG, "Error: " + throwable.getMessage());
//                Toast.makeText(Navigation.this, "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void drawRoute(DirectionsRoute route) {
        // Convert LineString coordinates into LatLng[]
        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        LatLng[] points = new LatLng[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(
                    coordinates.get(i).getLatitude(),
                    coordinates.get(i).getLongitude());
        }

        // Draw Points on MapView
        map.addPolyline(new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#009688"))
                .width(5));
    }

    private void drawAlternativeRoute(DirectionsRoute route){

        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        LatLng[] points = new LatLng[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(
                    coordinates.get(i).getLatitude(),
                    coordinates.get(i).getLongitude());
        }

        // Draw Points on MapView
        map.addPolyline(new PolylineOptions()
                .add(points)
                .color(Color.GRAY)
                .width(5));
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}