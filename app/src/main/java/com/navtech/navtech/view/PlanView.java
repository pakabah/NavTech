package com.navtech.navtech.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.navtech.navtech.MainActivity;
import com.navtech.navtech.R;
import com.navtech.navtech.db.DBHelper;
import com.navtech.navtech.model.Journey;
import com.navtech.navtech.model.PlainModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

public class PlanView extends AppCompatActivity {

    FloatingActionButton end;
    ImageView ground,first,second,third,fourth;
    TextView ground_text,first_text,second_text,third_text,fourth_text;
    PlainModel plainModel;
     DBHelper dbHelper;
    ImageView imageView;
    String placeId,floorId;

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {
                String status = bundle.getString("status");
                assert status != null;

                if(!floorId.isEmpty()){
                    plainModel =  dbHelper.getFloorWithID(floorId,placeId);
                }else{
                    plainModel =  dbHelper.getFloor("ground floor",placeId);
                }

//                Log.e("Plain Floor", plainModel.floor);

                switch (plainModel.floor){
                    case "ground floor":
                        ground.setImageResource(R.drawable.floor_plan_filled);
                        first.setImageResource(R.drawable.floor_plan);
                        second.setImageResource(R.drawable.floor_plan);
                        third.setImageResource(R.drawable.floor_plan);
                        fourth.setImageResource(R.drawable.floor_plan);

                        ground_text.setVisibility(View.VISIBLE);
                        first_text.setVisibility(View.INVISIBLE);
                        second_text.setVisibility(View.INVISIBLE);
                        third_text.setVisibility(View.INVISIBLE);
                        fourth_text.setVisibility(View.INVISIBLE);
                        break;
                    case "first floor":
                        ground.setImageResource(R.drawable.floor_plan);
                        first.setImageResource(R.drawable.floor_plan_filled);
                        second.setImageResource(R.drawable.floor_plan);
                        third.setImageResource(R.drawable.floor_plan);
                        fourth.setImageResource(R.drawable.floor_plan);

                        ground_text.setVisibility(View.INVISIBLE);
                        first_text.setVisibility(View.VISIBLE);
                        second_text.setVisibility(View.INVISIBLE);
                        third_text.setVisibility(View.INVISIBLE);
                        fourth_text.setVisibility(View.INVISIBLE);
                        break;
                    case "second floor":
                        ground.setImageResource(R.drawable.floor_plan);
                        first.setImageResource(R.drawable.floor_plan);
                        second.setImageResource(R.drawable.floor_plan_filled);
                        third.setImageResource(R.drawable.floor_plan);
                        fourth.setImageResource(R.drawable.floor_plan);

                        ground_text.setVisibility(View.INVISIBLE);
                        first_text.setVisibility(View.INVISIBLE);
                        second_text.setVisibility(View.VISIBLE);
                        third_text.setVisibility(View.INVISIBLE);
                        fourth_text.setVisibility(View.INVISIBLE);
                        break;
                    case "third floor":
                        ground.setImageResource(R.drawable.floor_plan);
                        first.setImageResource(R.drawable.floor_plan);
                        second.setImageResource(R.drawable.floor_plan);
                        third.setImageResource(R.drawable.floor_plan_filled);
                        fourth.setImageResource(R.drawable.floor_plan);

                        ground_text.setVisibility(View.INVISIBLE);
                        first_text.setVisibility(View.INVISIBLE);
                        second_text.setVisibility(View.INVISIBLE);
                        third_text.setVisibility(View.VISIBLE);
                        fourth_text.setVisibility(View.INVISIBLE);
                        break;
                    case "fourth floor":
                        ground.setImageResource(R.drawable.floor_plan);
                        first.setImageResource(R.drawable.floor_plan);
                        second.setImageResource(R.drawable.floor_plan);
                        third.setImageResource(R.drawable.floor_plan);
                        fourth.setImageResource(R.drawable.floor_plan_filled);

                        ground_text.setVisibility(View.INVISIBLE);
                        first_text.setVisibility(View.INVISIBLE);
                        second_text.setVisibility(View.INVISIBLE);
                        third_text.setVisibility(View.INVISIBLE);
                        fourth_text.setVisibility(View.VISIBLE);
                        break;
                    default:
                        ground.setImageResource(R.drawable.floor_plan_filled);
                        first.setImageResource(R.drawable.floor_plan);
                        second.setImageResource(R.drawable.floor_plan);
                        third.setImageResource(R.drawable.floor_plan);
                        fourth.setImageResource(R.drawable.floor_plan);

                        ground_text.setVisibility(View.VISIBLE);
                        first_text.setVisibility(View.INVISIBLE);
                        second_text.setVisibility(View.INVISIBLE);
                        third_text.setVisibility(View.INVISIBLE);
                        fourth_text.setVisibility(View.INVISIBLE);
                        break;
                }

                Picasso.with(getApplicationContext())
                        .load(plainModel.Image)
                        .into(imageView);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_view);

        registerReceiver(broadcastReceiver, new IntentFilter(Journey.ROOM_FLOOR));

        end = (FloatingActionButton) findViewById(R.id.end);

        ground = (ImageView) findViewById(R.id.ground);
        first = (ImageView) findViewById(R.id.first);
        second = (ImageView) findViewById(R.id.second);
        third = (ImageView) findViewById(R.id.third);
        fourth = (ImageView) findViewById(R.id.fourth);

        ground_text = (TextView) findViewById(R.id.ground_text);
        first_text = (TextView) findViewById(R.id.first_text);
        second_text = (TextView) findViewById(R.id.second_text);
        third_text = (TextView) findViewById(R.id.third_text);
        fourth_text = (TextView) findViewById(R.id.fourth_text);

        Bundle bundle = getIntent().getExtras();
         placeId = bundle.getString("searchId");
         floorId = bundle.getString("searchFloor");
        assert floorId != null;
        dbHelper = new DBHelper(getApplicationContext());

        if(!floorId.isEmpty()){
            plainModel =  dbHelper.getFloorWithID(floorId,placeId);
        }else{
            plainModel =  dbHelper.getFloor("ground floor",placeId);
        }

        imageView = (ImageView)findViewById(R.id.imageView9);



        Picasso.with(getApplicationContext())
                .load(plainModel.Image)
                .into(imageView);

        ImageView zoom = (ImageView) findViewById(R.id.zoom);
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap image=((BitmapDrawable)imageView.getDrawable()).getBitmap();
                createImageFromBitmap(image);

                Intent intent = new Intent(getApplicationContext(),ZoomView.class);
                startActivity(intent);
            }
        });

        ground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plainModel =  dbHelper.getFloor("ground floor",placeId);

                if(plainModel.Image != null){
                    Picasso.with(getApplicationContext())
                            .load(plainModel.Image)
                            .into(imageView);
                }else{
                    Toast.makeText(getApplicationContext(),"No Floor",Toast.LENGTH_LONG).show();
                }
                
                ground.setImageResource(R.drawable.floor_plan_filled);
                first.setImageResource(R.drawable.floor_plan);
                second.setImageResource(R.drawable.floor_plan);
                third.setImageResource(R.drawable.floor_plan);
                fourth.setImageResource(R.drawable.floor_plan);

                ground_text.setVisibility(View.VISIBLE);
                first_text.setVisibility(View.INVISIBLE);
                second_text.setVisibility(View.INVISIBLE);
                third_text.setVisibility(View.INVISIBLE);
                fourth_text.setVisibility(View.INVISIBLE);

            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plainModel =  dbHelper.getFloor("first floor",placeId);

                if(plainModel.Image != null){
                    Picasso.with(getApplicationContext())
                            .load(plainModel.Image)
                            .into(imageView);
                }else{
                    Toast.makeText(getApplicationContext(),"No Floor",Toast.LENGTH_LONG).show();
                }

                ground.setImageResource(R.drawable.floor_plan);
                first.setImageResource(R.drawable.floor_plan_filled);
                second.setImageResource(R.drawable.floor_plan);
                third.setImageResource(R.drawable.floor_plan);
                fourth.setImageResource(R.drawable.floor_plan);

                ground_text.setVisibility(View.INVISIBLE);
                first_text.setVisibility(View.VISIBLE);
                second_text.setVisibility(View.INVISIBLE);
                third_text.setVisibility(View.INVISIBLE);
                fourth_text.setVisibility(View.INVISIBLE);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plainModel =  dbHelper.getFloor("second floor",placeId);
                if(plainModel.Image != null){
                    Picasso.with(getApplicationContext())
                            .load(plainModel.Image)
                            .into(imageView);
                }else{
                    Toast.makeText(getApplicationContext(),"No Floor",Toast.LENGTH_LONG).show();
                }

                ground.setImageResource(R.drawable.floor_plan);
                first.setImageResource(R.drawable.floor_plan);
                second.setImageResource(R.drawable.floor_plan_filled);
                third.setImageResource(R.drawable.floor_plan);
                fourth.setImageResource(R.drawable.floor_plan);

                ground_text.setVisibility(View.INVISIBLE);
                first_text.setVisibility(View.INVISIBLE);
                second_text.setVisibility(View.VISIBLE);
                third_text.setVisibility(View.INVISIBLE);
                fourth_text.setVisibility(View.INVISIBLE);
            }
        });

        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plainModel =  dbHelper.getFloor("third floor",placeId);
                if(plainModel.Image != null){
                    Picasso.with(getApplicationContext())
                            .load(plainModel.Image)
                            .into(imageView);
                }else{
                    Toast.makeText(getApplicationContext(),"No Floor",Toast.LENGTH_LONG).show();
                }

                ground.setImageResource(R.drawable.floor_plan);
                first.setImageResource(R.drawable.floor_plan);
                second.setImageResource(R.drawable.floor_plan);
                third.setImageResource(R.drawable.floor_plan_filled);
                fourth.setImageResource(R.drawable.floor_plan);

                ground_text.setVisibility(View.INVISIBLE);
                first_text.setVisibility(View.INVISIBLE);
                second_text.setVisibility(View.INVISIBLE);
                third_text.setVisibility(View.VISIBLE);
                fourth_text.setVisibility(View.INVISIBLE);
            }
        });

        fourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                plainModel =  dbHelper.getFloor("fourth floor",placeId);
                if(plainModel.Image != null){
                    Picasso.with(getApplicationContext())
                            .load(plainModel.Image)
                            .into(imageView);
                }else{
                    Toast.makeText(getApplicationContext(),"No Floor",Toast.LENGTH_LONG).show();
                }

                ground.setImageResource(R.drawable.floor_plan);
                first.setImageResource(R.drawable.floor_plan);
                second.setImageResource(R.drawable.floor_plan);
                third.setImageResource(R.drawable.floor_plan);
                fourth.setImageResource(R.drawable.floor_plan_filled);

                ground_text.setVisibility(View.INVISIBLE);
                first_text.setVisibility(View.INVISIBLE);
                second_text.setVisibility(View.INVISIBLE);
                third_text.setVisibility(View.INVISIBLE);
                fourth_text.setVisibility(View.VISIBLE);
            }
        });


        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}