package com.navtech.navtech.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.navtech.navtech.R;
import com.navtech.navtech.db.DBHelper;
import com.navtech.navtech.model.Journey;
import com.navtech.navtech.template.MainTemplate;

public class LocationDetails extends AppCompatActivity {

    private String token = "pk.eyJ1Ijoic3BlY2tvcmJpdGVyIiwiYSI6ImNpdjUweXF4djAwMDgydHFteXpsa3M4MTIifQ.rnyZJm6XA6tIjXGA6wBuAQ";
    private MapView mapView;
     String place,origin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle bundle = getIntent().getExtras();
         place = bundle.getString("searchTerm");
         origin = bundle.getString("searchFrom");

        MapboxAccountManager.start(this, token);
        setContentView(R.layout.activity_location_details);

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.setAccessToken(token);
        mapView.setStyleUrl("mapbox://styles/speckorbiter/civ512d8s001c2il6a10pqu66");
        mapView.onCreate(savedInstanceState);

        TextView textView = (TextView) findViewById(R.id.searchRes);
        final Button startNavigation = (Button) findViewById(R.id.start);

        TextView originView = (TextView) findViewById(R.id.origin);
        originView.setText(origin);

        DBHelper dbHelper = new DBHelper(getApplicationContext());
       dbHelper.deleteAllRooms();

        startNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DBHelper dbHelper = new DBHelper(getApplicationContext());
                final MainTemplate mainTemplate =  dbHelper.getSinglePlace(place);
                Journey journey = new Journey(getApplicationContext());
                Log.e("Room ID", mainTemplate.Id);
                journey.getRooms(mainTemplate.Id);

                Intent intent = new Intent(getApplicationContext(), Navigation.class);
                intent.putExtra("searchTerm", place);
                intent.putExtra("searchFrom", origin);
                startActivity(intent);
            }
        });

        textView.setText(place);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        Log.e("On Low Memory", "Low memory");
//        mapView.onLowMemory();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}