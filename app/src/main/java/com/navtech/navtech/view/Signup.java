package com.navtech.navtech.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.navtech.navtech.MainActivity;
import com.navtech.navtech.R;
import com.navtech.navtech.model.User;

public class Signup extends AppCompatActivity {

    private FirebaseAuth mAuth;
    ProgressDialog progress;
    User mUser;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private static final String TAG = "EmailPassword";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Button signup = (Button) findViewById(R.id.button2);

        mAuth = FirebaseAuth.getInstance();
        mUser = new User(getApplicationContext());

        final EditText name = (EditText) findViewById(R.id.name);
       final EditText email = (EditText) findViewById(R.id.email);
       final EditText password = (EditText) findViewById(R.id.password);
       final EditText repassword = (EditText) findViewById(R.id.repassword);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(name.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Name is required", Toast.LENGTH_LONG).show();
                }
                else if(email.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Email is required", Toast.LENGTH_LONG).show();
                }
                else if(password.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Password is required", Toast.LENGTH_LONG).show();
                }
                else if(repassword.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Re-Enter Password is required", Toast.LENGTH_LONG).show();
                }
                else if(!password.getText().toString().equals(repassword.getText().toString()))
                {
                    Toast.makeText(getApplicationContext(),"Passwords are not the same", Toast.LENGTH_LONG).show();
                }
                else
                {

                    progress = new ProgressDialog(Signup.this);
                    progress.setTitle("Sign Up");
                    progress.setMessage("Signing Up...");
                    progress.show();

                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(Signup.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        progress.dismiss();
                                        Toast.makeText(getApplicationContext(),"Signup Unsuccessful", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });

        TextView login = (TextView) findViewById(R.id.loginText);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {


                    SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("login", "1");
                    editor.apply();

                    mUser.setUserId(user.getUid());
                    mUser.setUserName(user.getDisplayName());
                    mUser.setUserEmail(user.getEmail());

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
