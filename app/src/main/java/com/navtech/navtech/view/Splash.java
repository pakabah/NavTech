package com.navtech.navtech.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.navtech.navtech.MainActivity;
import com.navtech.navtech.R;
import com.navtech.navtech.model.Journey;
import com.navtech.navtech.service.DbService;

import io.fabric.sdk.android.Fabric;

import static com.navtech.navtech.MainActivity.DEFAULT;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        String  once = sharedPreferences.getString("once", DEFAULT);

        if(!once.equals("1"))
        {
            Intent intent = new Intent(this, DbService.class);
            startService(intent);


            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("once", "1");
            editor.apply();
        }


        Thread thread = new Thread(){
            @Override
            public void run()
            {
                try{
                    int waited = 0;

                    while (waited < 3500) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                }
                catch(InterruptedException e)
                {

                }
            }
        };
        thread.start();
    }
}
