package com.navtech.navtech.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.navtech.navtech.model.PlainModel;
import com.navtech.navtech.template.MainTemplate;
import com.navtech.navtech.template.RoomTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pakabah on 29/10/2016.
 */

public class DBHelper {

    private Places places;

    public DBHelper(Context context) {
        places = new Places(context);
    }

    public void insertPlaces(ContentValues contentValues) {
        SQLiteDatabase db = places.getWritableDatabase();
        db.insert(Places.TABLE_LOCATION,null,contentValues);
        db.close();
    }

    public void insertLayout(ContentValues contentValues){
        SQLiteDatabase db = places.getWritableDatabase();
        db.insert(Places.TABLE_LAYOUTS,null,contentValues);
        db.close();
    }

    public void insertRooms(ContentValues contentValues){
        SQLiteDatabase db  = places.getWritableDatabase();
        db.insert(Places.TABLE_ROOMS,null,contentValues);
        db.close();
    }

    public void deleteAllPlaces() {

        SQLiteDatabase db = places.getWritableDatabase();
        db.delete(Places.TABLE_LOCATION, null, null);
        db.close();
    }

    public void deleteAllLayouts(){
        SQLiteDatabase db = places.getWritableDatabase();
        db.delete(Places.TABLE_LAYOUTS,null,null);
        db.close();
    }

    public void deleteAllRooms(){
        SQLiteDatabase db = places.getWritableDatabase();
        db.delete(Places.TABLE_ROOMS,null,null);
        db.close();
    }


    public List<MainTemplate> getPlace(String searchTerm) {
        List<MainTemplate> mainTemplates = new ArrayList<>();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns= {Places.COLUMN_PLACE_NAME,Places.COLUMN_PLACE_LONG,Places.COLUMN_PLACE_LAT,Places.COLUMN_ID};
        Cursor cursor = db.query(Places.TABLE_LOCATION, columns, Places.COLUMN_PLACE_NAME + " LIKE '%" + searchTerm + "%'", null, null, null, null);

        while(cursor.moveToNext()) {
            int index  = cursor.getColumnIndex(Places.COLUMN_PLACE_NAME);
            int index1 = cursor.getColumnIndex(Places.COLUMN_PLACE_LAT);
            int index2 = cursor.getColumnIndex(Places.COLUMN_PLACE_LONG);
            int index3 = cursor.getColumnIndex(Places.COLUMN_ID);

            MainTemplate mainTemplate = new MainTemplate();

            mainTemplate.Name = cursor.getString(index);
            mainTemplate.Lat = cursor.getString(index1);
            mainTemplate.Long = cursor.getString(index2);
            mainTemplate.Id = cursor.getString(index3);

            mainTemplates.add(mainTemplate);
        }

        db.close();
        return  mainTemplates;
    }


    public List<RoomTemplate> getRooms(String layout_id){
        List<RoomTemplate> roomTemplates = new ArrayList<>();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns = {Places.COLUMN_FLOOR_ID,Places.COLUMN_ROOM_NAME,Places.COLUMN_LAYOUT_ROOM_ID,Places.COLUMN_LAYOUT_PLACE_ID};

        String where = Places.COLUMN_LAYOUT_PLACE_ID+" = ?";
        String[] whereArgs = new String[]{layout_id};

        Cursor cursor = db.query(Places.TABLE_ROOMS, columns, where,whereArgs,null,null,null);

        while(cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_ROOM_NAME);
            int index2 = cursor.getColumnIndex(Places.COLUMN_FLOOR_ID);
            int index3 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_ROOM_ID);
            int index4 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PLACE_ID);

            RoomTemplate roomTemplate = new RoomTemplate();

            roomTemplate.RoomName = cursor.getString(index);
            roomTemplate.floorId = cursor.getString(index2);
            roomTemplate.PlaceId = cursor.getString(index4);
            roomTemplate.RoomId = cursor.getString(index3);

            roomTemplates.add(roomTemplate);
        }
        db.close();
        return roomTemplates;
    }

    public RoomTemplate getRoomId(String room_name){

        RoomTemplate roomTemplate = new RoomTemplate();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns = {Places.COLUMN_FLOOR_ID,Places.COLUMN_ROOM_NAME,Places.COLUMN_LAYOUT_ROOM_ID,Places.COLUMN_LAYOUT_PLACE_ID};

        Cursor cursor = db.query(Places.TABLE_ROOMS, columns, Places.COLUMN_ROOM_NAME + " LIKE '%" + room_name + "%'",null,null,null,null);

        while(cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_ROOM_NAME);
            int index2 = cursor.getColumnIndex(Places.COLUMN_FLOOR_ID);
            int index3 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_ROOM_ID);
            int index4 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PLACE_ID);

            roomTemplate.RoomName = cursor.getString(index);
            roomTemplate.floorId = cursor.getString(index2);
            roomTemplate.PlaceId = cursor.getString(index4);
            roomTemplate.RoomId = cursor.getString(index3);

        }
        db.close();
        return roomTemplate;
    }


    public List<String> getRoomsString(String layout_id){

        List<String> roomTemplates = new ArrayList<>();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns = {Places.COLUMN_FLOOR_ID,Places.COLUMN_ROOM_NAME,Places.COLUMN_LAYOUT_ROOM_ID,Places.COLUMN_LAYOUT_PLACE_ID};

        String where = Places.COLUMN_LAYOUT_PLACE_ID+" = ?";
        String[] whereArgs = new String[]{layout_id};

        Cursor cursor = db.query(Places.TABLE_ROOMS, columns, where,whereArgs,null,null,null);

        while(cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_ROOM_NAME);
            int index2 = cursor.getColumnIndex(Places.COLUMN_FLOOR_ID);
            int index3 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_ROOM_ID);
            int index4 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PLACE_ID);

            RoomTemplate roomTemplate = new RoomTemplate();

            String RoomName = cursor.getString(index);
//            roomTemplate.floorId = cursor.getString(index2);
//            roomTemplate.PlaceId = cursor.getString(index4);
//            roomTemplate.RoomId = cursor.getString(index3);

            roomTemplates.add(RoomName);
//            Log.e("Room Name", RoomName);
        }
        db.close();
        return roomTemplates;
    }

    public MainTemplate getPlaceDetails(String searchTerm) {

        MainTemplate mainTemplates = new MainTemplate();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns= {Places.COLUMN_PLACE_NAME,Places.COLUMN_PLACE_LONG,Places.COLUMN_PLACE_LAT,Places.COLUMN_ID};
        Cursor cursor = db.query(Places.TABLE_LOCATION, columns, Places.COLUMN_PLACE_NAME + " LIKE '%" + searchTerm + "%'", null, null, null, null);

        while(cursor.moveToNext()) {
            int index  = cursor.getColumnIndex(Places.COLUMN_PLACE_NAME);
            int index1 = cursor.getColumnIndex(Places.COLUMN_PLACE_LAT);
            int index2 = cursor.getColumnIndex(Places.COLUMN_PLACE_LONG);
            int index3 = cursor.getColumnIndex(Places.COLUMN_ID);

            mainTemplates.Name = cursor.getString(index);
            mainTemplates.Lat = cursor.getString(index1);
            mainTemplates.Long = cursor.getString(index2);
            mainTemplates.Id = cursor.getString(index3);
        }

        db.close();
        return  mainTemplates;
    }

    public MainTemplate getSinglePlace(String searchTerm){

        MainTemplate mainTemplate = new MainTemplate();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns= {Places.COLUMN_PLACE_NAME,Places.COLUMN_PLACE_LONG,Places.COLUMN_PLACE_LAT,Places.COLUMN_ID};
        Cursor cursor = db.query(Places.TABLE_LOCATION, columns, Places.COLUMN_PLACE_NAME + " LIKE '%" + searchTerm + "%'", null, null, null, null);

        while(cursor.moveToNext()) {
            int index  = cursor.getColumnIndex(Places.COLUMN_PLACE_NAME);
            int index1 = cursor.getColumnIndex(Places.COLUMN_PLACE_LAT);
            int index2 = cursor.getColumnIndex(Places.COLUMN_PLACE_LONG);
            int index3 = cursor.getColumnIndex(Places.COLUMN_ID);

            mainTemplate.Name = cursor.getString(index);
            mainTemplate.Lat = cursor.getString(index1);
            mainTemplate.Long = cursor.getString(index2);
            mainTemplate.Id = cursor.getString(index3);
        }

        db.close();
        return  mainTemplate;
    }

    public PlainModel getImage(String placeId){
        PlainModel plainModel = new PlainModel();
        SQLiteDatabase db = places.getWritableDatabase();
        String[] columns = {Places.COLUMN_LAYOUT_PICTURE,Places.COLUMN_LAYOUT_FLOOR};

        String where = Places.COLUMN_LAYOUT_PLACE_ID+" = ? AND "+Places.COLUMN_LAYOUT_FLOOR+" = ?";
        String[] whereArgs = new String[]{placeId, "0"};

        Cursor cursor = db.query(Places.TABLE_LAYOUTS,columns,where,whereArgs,null,null,null);

        while (cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_LAYOUT_FLOOR);
            int index1 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PICTURE);

          plainModel.Image = cursor.getString(index1);
            plainModel.floor = cursor.getString(index);
        }

        return plainModel;
    }

    public PlainModel getFloor(String floor,String placeId){


        PlainModel plainModel = new PlainModel();
        SQLiteDatabase db = places.getWritableDatabase();

        String[] columns = {Places.COLUMN_LAYOUT_PICTURE,Places.COLUMN_LAYOUT_FLOOR};

        String where = Places.COLUMN_LAYOUT_PLACE_ID+" = ? AND "+Places.COLUMN_LAYOUT_FLOOR+" = ?";
        String[] whereArgs = new String[]{placeId, floor};

        Cursor cursor = db.query(Places.TABLE_LAYOUTS,columns,where,whereArgs,null,null,null);

        while (cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_LAYOUT_FLOOR);
            int index1 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PICTURE);

            plainModel.Image = cursor.getString(index1);
            plainModel.floor = cursor.getString(index);
        }

        return plainModel;
    }

    public PlainModel getFloorWithID(String floor_id,String placeId){


        PlainModel plainModel = new PlainModel();
        SQLiteDatabase db = places.getWritableDatabase();

        String[] columns = {Places.COLUMN_LAYOUT_PICTURE,Places.COLUMN_LAYOUT_FLOOR};

        String where = Places.COLUMN_LAYOUT_PLACE_ID+" = ? AND "+Places.COLUMN_FLOOR_ID+" = ?";
        String[] whereArgs = new String[]{placeId, floor_id};

        Cursor cursor = db.query(Places.TABLE_LAYOUTS,columns,where,whereArgs,null,null,null);

        while (cursor.moveToNext()){
            int index = cursor.getColumnIndex(Places.COLUMN_LAYOUT_FLOOR);
            int index1 = cursor.getColumnIndex(Places.COLUMN_LAYOUT_PICTURE);

            plainModel.Image = cursor.getString(index1);
            plainModel.floor = cursor.getString(index);
        }

        return plainModel;
    }


    public class Places extends SQLiteOpenHelper
    {
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "navtech.db";

        public static final String TABLE_LOCATION = "locations";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_PLACE_NAME = "place_name";
        public static final String COLUMN_PLACE_LONG = "place_long";
        public static final String COLUMN_PLACE_LAT = "place_lat";

        public static final String TABLE_LAYOUTS = "layouts";
        public static final String COLUMN_LAYOUT_ID = "_ids";
        public static final String COLUMN_LAYOUT_PLACE_ID = "place";
        public static final String COLUMN_LAYOUT_FLOOR = "floor";
        public static final String COLUMN_LAYOUT_PICTURE = "picture";

        public static final String TABLE_ROOMS = "room_layouts";
        public static final String COLUMN_LAYOUT_ROOM_ID = "room_id";
        public static final String COLUMN_ROOM_NAME = "room_name";
        public static final String COLUMN_FLOOR_ID = "floor_id";


        public static final String DATABASE_CREATE_PLACES = "create table "
                + TABLE_LOCATION + "(" + COLUMN_ID
                + " INTEGER PRIMARY KEY, "
                + COLUMN_PLACE_LAT + " text, "
                + COLUMN_PLACE_LONG + " text, "
                + COLUMN_PLACE_NAME + " text not null );";

        public static final String DATABASE_CREATE_LAYOUTS = "create table "
                + TABLE_LAYOUTS + "(" + COLUMN_LAYOUT_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LAYOUT_PLACE_ID + " text, "
                + COLUMN_LAYOUT_FLOOR + " text, "
                + COLUMN_FLOOR_ID + " text, "
                + COLUMN_LAYOUT_PICTURE + " text );";

        public static final String DATABASE_CREATE_ROOMS = "create table "
                + TABLE_ROOMS + "(" + COLUMN_ID
                +  " INTEGER PRIMARY KEY, "
                + COLUMN_LAYOUT_PLACE_ID + " text, "
                + COLUMN_LAYOUT_ROOM_ID + " text, "
                + COLUMN_FLOOR_ID + " text, "
                + COLUMN_ROOM_NAME + " text );";

        public Places(Context context) {
            super(context,DATABASE_NAME,null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE_PLACES);
            db.execSQL(DATABASE_CREATE_LAYOUTS);
            db.execSQL(DATABASE_CREATE_ROOMS);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_LAYOUTS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOMS);
        }
    }
}