package com.navtech.navtech.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.navtech.navtech.R;
import com.navtech.navtech.template.MainTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pakabah on 29/10/2016.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    List<MainTemplate> mainTemplates;

    public MainAdapter(List<MainTemplate> mainTemplates) {
        this.mainTemplates = new ArrayList<>();
        this.mainTemplates.addAll(mainTemplates);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_search,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MainTemplate mainTemplate = mainTemplates.get(position);

        holder.name.setText(mainTemplate.Name);
        holder.name.setTag(mainTemplate.Id);
    }

    @Override
    public int getItemCount() {
        return mainTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            cardView = (CardView) itemView.findViewById(R.id.searchCard);
        }
    }
}
