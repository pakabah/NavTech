package com.navtech.navtech.core;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;

import com.navtech.navtech.MainActivity;
import com.navtech.navtech.db.DBHelper;
import com.navtech.navtech.template.MainTemplate;

import java.util.List;

/**
 * Created by akabah on 10/25/15.
 */
public class CustomAuto implements TextWatcher {

    Context context;
    MainActivity mainActivity;

    public CustomAuto(Context context)
    {
        this.context = context;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }


    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        mainActivity = (MainActivity) context;
        mainActivity.item = getItemsFromDb(s.toString(), context);
        mainActivity.items = getItemsFromDb(s.toString(), context);

        mainActivity.adapter.notifyDataSetChanged();
        mainActivity.adapters.notifyDataSetChanged();

        mainActivity.adapter = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_dropdown_item_1line, mainActivity.item);
        mainActivity.adapters = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_dropdown_item_1line, mainActivity.item);

        mainActivity.customAutoView.setAdapter(mainActivity.adapter);
        mainActivity.fromCustomAutoView.setAdapter(mainActivity.adapters);

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public String[] getItemsFromDb(String searchTerm, Context context){

        // add items on the array dynamically
        DBHelper dbHelper = new DBHelper(context);

        List<MainTemplate> places = dbHelper.getPlace(searchTerm);

        int rowCount = places.size();

        String[] item = new String[rowCount];
        int x = 0;

        for (MainTemplate record : places) {

            item[x] = record.Name;
            x++;
        }

        return item;
    }


}
